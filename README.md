# Biomex SDLC deployed with Jenkins X on Minikube.
TEST PR EDIT
Install minikube and jx using the links to the resources below.

## Resources.

Links to useful resources and documentation.

```
# Jenkins X.

https://jenkins-x.io/getting-started/create-cluster/#using-minikube-local
https://github.com/jenkins-x

# Minikube.

https://kubernetes.io/docs/tasks/tools/install-minikube/
https://github.com/kubernetes/minikube
```

## Known issues.

```
Use writethrough disk caching on KVM's.
```

## ToDo.

```
Shell auto completion for minikube and jx.
Developer workflow - create issue, branch feature, commit & push, pull request. 
Maintainer workflow - list pull request, branch feature, merge feature, commit & push to master.
``` 

## Clone this repo. 

Use the ssh protocol and your attlassion user name.

```
git clone git@bitbucket.org:<username>/sdlc-circuit.git
```

Edit the 0-jx-create file with github credentials and api key.

```
--default-admin-password='<password>'
--git-api-token='<token>'
--git-username='<username>'

--username='<username>'
--password='<password>'
```

## Create the K8s cluster

```
cd sdlc-crcuit/deploy/linux/gitea
./0-jx-create
```

Edit the 1-jx-install file with the gitea creadentials and api key.

```
--default-admin-password='<password>' 
--git-username='<username>' 
--git-api-token='<token>' 
--git-provider-url='http://gitea-gitea.jx.<ip_address>.nip.io'
```

## Install jx

```
1-jx-install
```

## Verfify the installation.

```
minikube status
jx status
docker images
kubectl get po
```

## Access to consoles.
```
minikube dashboard
jx open jenkins
```

## Import the biomex repos and attempt a build.

```
jx import --no-draft=true --no-jenkinsfile=true <repo>
```

## Cluster control options.

Stop the cluster.

```
minikube stop
```

Start the cluster.

```
minikube start
```

Uninstall the cluster.

```
./2-jx-uninstall
```

Clean up repos.

```
rm -rf <repo>/charts <repo>/.git
```
